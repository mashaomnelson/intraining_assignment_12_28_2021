IN-TRAINING ASSIGNMENT (12/28/2021):
 
 I'd like you to do this while in your breakout rooms. I'd like you to build a small applicataion. Walk throough the following steps.
 
 -Create a NEW database in your AWS:RDS instance (using DBeaver).
 -In this database create a "clothing" table that keeps track of a clothing's type, it's brand name, it's color, and it's size. Type being t-shirt, button up shirt, dress, skinny jeans, etc.
-Populate the table with some starter data.
-In your SQL script you may want to form it in a way that you can run the script and it'll start by dropping the tables, creatings the tables, then populating the tables; this way you can easily restart from square one with the click of a button. 
 -Create a Maven project in STS. Setup your pom.xml file appropriately
 -Create a DAO layer that performs basic CRUD methods on your clothing database using JDBC
 -Create a service layer that rejects any input to the database that is less than 3 characters in length, and any input that is greater than 20 characters in length. When it "rejects" the input the method can simply result false or an empty object or null or whatever else you need it to reject to signal to you that the transaction didn't go through.
 -Finally, create a simple event while loop that let's you insert new clothing, see all existing clothing, update a clothing, and delete a clothing. AGain, simple crud methods. If you need help with this then go look at my "SimilarToProject00Part01" example.